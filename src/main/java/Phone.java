public class Phone{

    private String name;
    private String company;
    private int price;


    public Phone(String name, String company, int price){
        this.name=name;
        this.company=company;
        this.price=price;

    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name=name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getCompany() {
        return company;
    }

    public static int compare (Phone p1, Phone p2){
        if(p1.getPrice() > p2.getPrice())
            return 1;
        return -1;

    }
}
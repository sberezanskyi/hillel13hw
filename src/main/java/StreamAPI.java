import java.lang.reflect.Array;
import java.util.*;
import java.util.function.Function;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class StreamAPI {
    public static void main(String[] args) {
        // ******* Введение

        long count = IntStream.of(-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5).filter(w->w>0).count();
        System.out.println(count);


        // ******* Создание потока

        ArrayList<String> cities = new ArrayList<String>();
        Collections.addAll(cities, "Париж", "Лондон", "Мадрид");
//        cities.stream()
//                .filter(s->s.length()==6)
//                .forEach(s-> System.out.println(s));


//        Stream<String> citiesStream = cities.stream();
//        citiesStream = citiesStream.filter(s -> s.length()==6);
//        citiesStream.forEach(s -> System.out.println(s));

//        long number = citiesStream.count();
//        System.out.println(number);
//        citiesStream = citiesStream.filter(s -> s.length()>5);

//        Stream<String> citiesStream = Arrays.stream(new String[]{"Париж", "Лондон", "Мадрид"});
//        citiesStream.forEach(s -> System.out.println(s));

        IntStream intStream = Arrays.stream(new int[]{1,2,4,5,7});
        intStream.forEach(i-> System.out.println(i));

        LongStream longStream = Arrays.stream(new long[]{100,250,400,5843787,237});
        longStream.forEach(l -> System.out.println(l) );

        DoubleStream doubleStream = Arrays.stream(new double[]{3.4, 6.7, 9.5, 8.2345, 121});
        doubleStream.forEach(d -> System.out.println(d));

        Stream<String> citiesStream = Stream.of("Париж", "Лондон", "Мадрид");
        citiesStream.forEach(s-> System.out.println(s));

        String[] cities2 = {"Париж", "Лондон", "Мадрид"};
        Stream<String> citiesStream2 = Stream.of(cities2);

        IntStream intStream2 = IntStream.of(1,2,4,5,7);
        intStream2.forEach(i-> System.out.println(i));

        LongStream longStream2 = LongStream.of(100,250,400,5843787,237);
        longStream2.forEach(l-> System.out.println(l));

        DoubleStream doubleStream2 = DoubleStream.of(3.4, 6.7, 9.5, 8.2345, 121);
        doubleStream2.forEach(d-> System.out.println(d));


        // ******* Фильтрация, перебор элементов и отображение

        Stream<String> citiesStreamF = Stream.of("---------------","Париж", "Лондон", "Мадрид", "Берлин", "Брюссель");
        citiesStreamF.forEach(s-> System.out.println(s));

        Stream<String> citiesStreamFF = Stream.of("++++++++++++++", "Париж", "Лондон", "Мадрид", "Берлин", "Брюссель");
        citiesStreamFF.forEach(System.out::println);

        Stream<String> citiesStreamFFF = Stream.of("******","Париж", "Лондон", "Мадрид", "Берлин", "Брюссель");
        citiesStreamFFF.filter(s -> s.length()==6).forEach(s-> System.out.println(s));




//        Stream<Phone> phoneStream = Stream.of(new Phone("iPhone 6 S", 54000),
//                new Phone("Lumia 950", 45000),
//                new Phone("Samsung Galaxy S 6", 40000));

//        phoneStream.filter(p->p.getPrice()<50000).forEach(phone -> System.out.println(phone.getName()));


      //  <R> Stream<R> map(Function <? super T, ? extends R> mapper)

//        phoneStream
//                .map(p-> p.getName())
//                .forEach(s -> System.out.println(s));
//
//        phoneStream
//                .map(p -> "Название: " + p.getName() + "Цена: " + p.getPrice())
//                .forEach(s-> System.out.println(s));


//        <R> Stream<R> flatMap(Function<? super T, ? extends Stream <? extends R>> mapper)


//        Stream<Phone> phoneStream = Stream.of(new Phone("iPhone 6 S", 54000),
//                new Phone("Lumia 950", 45000),
//                new Phone("Samsung Galaxy S 6", 40000));

//        phoneStream
//                .flatMap(p->Stream.of(
//                        String.format("название: %s  цена без скидки: %d", p.getName(), p.getPrice()),
//                        String.format("название: %s  цена без скидки: %d", p.getName(), p.getPrice()-(int)(p.getPrice()*0.1))
//                ))
//                .forEach(s -> System.out.println(s));




        // ******* Сортировка


        List<String> phones = new ArrayList<String>();
        Collections.addAll(phones, "iPhone X", "Nokia 9", "Huawei Nexus 6P",
                "Samsung Galaxy S8", "LG G6", "Xiaomi MI6", "ASUS Zenfone 3",
                "Sony Xperia Z5", "Meizu Pro 6", "Pixel 2");

//        phones.stream()
//                .filter(p->p.length()<12)
//                .sorted()
//                .forEach(s-> System.out.println(s));


        Stream<Phone> phoneStream = Stream.of(
                new Phone("iPhone X", "Apple", 600),
                new Phone("Pixel 2", "Google", 500),
                new Phone("iPhone 8", "Apple",450),
                new Phone("Nokia 9", "HMD Global",150),
                new Phone("Galaxy S9", "Samsung", 300));

        phoneStream.sorted(new PhoneComparator())
                  .forEach(p->System.out.printf("%s (%s) - %d \n",
                        p.getName(), p.getCompany(), p.getPrice()));


        // ******* Получение подпотока и объединение потоков

        Stream<Integer> numbers = Stream.of(-3, -2, -1, 0, 1, 2, 3, -4, -5);
//        numbers.takeWhile(n -> n<0)
//                .forEach(n-> System.out.println(n));

//        numbers.sorted().takeWhile(n -> n<0)
//                .forEach(n -> System.out.println(n));

        numbers.sorted().dropWhile(n -> n<0)
                .forEach(n -> System.out.println(n));


        Stream<String> people1 = Stream.of("Tom", "Bob", "Sam");
        Stream<String> people2 = Stream.of("Alice", "Kate", "Sam");
        Stream<String> people = Stream.of("Tom", "Bob", "Sam", "Tom", "Alice", "Kate", "Sam");

        Stream.concat(people1,people2).forEach(n-> System.out.println(n));
        people.distinct().forEach(p-> System.out.println(p));

        // ******* Методы skip и limit

        Stream<String> phoneStream3 = Stream.of("iPhone 6 S", "Lumia 950", "Samsung Galaxy S 6", "LG G 4", "Nexus 7");

        phoneStream3.skip(1).limit(2).forEach(s-> System.out.println(s));


        phones.addAll(Arrays.asList(new String[]{
                "iPhone 6 S", "Lumia 950", "Huawei Nexus 6P",
                "Samsung Galaxy S 6", "LG G 4", "Xiaomi MI 5",
                "ASUS Zenfone 2", "Sony Xperia Z5", "Meizu Pro 5",
                "Lenovo S 850"}));
        int pageSize =3;
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Введите номер страницы: ");
            int page = scanner.nextInt();

            if (page < 1) break;

            phones.stream().skip((page - 1) * pageSize)
                    .limit(pageSize)
                    .forEach(s -> System.out.println(s));
            }

            // ******* Операции сведения

            ArrayList<String> names = new ArrayList<String>();
            names.addAll(Arrays.asList(new String[]{"Tom", "Sam", "Bob", "Alice"}));
            System.out.println(names.stream().count());
            System.out.println(names.stream().filter(n->n.length()<=3).count());

            Optional<String> first = names.stream().findFirst();
            System.out.println(first.get());

            Optional<String> any = names.stream().findAny();
            System.out.println(first.get());

            boolean any2 = names.stream().anyMatch(s -> s.length()>3);
            System.out.println(any2);

            boolean all2 = names.stream().allMatch(s -> s.length()==3);
            System.out.println(all2);

            boolean none = names.stream().noneMatch(s -> s=="Bill");
            System.out.println(none);



            ArrayList<Integer> numbers2 = new ArrayList<Integer>();
            numbers2.addAll(Arrays.asList(new Integer[]{1,2,3,4,5,6,7,8,9}));

            Optional<Integer> min = numbers2.stream().min(Integer::compare);
            Optional<Integer> max = numbers2.stream().max(Integer::compare);
            System.out.println(min.get());
            System.out.println(max.get());


            ArrayList<Phone> phones2 = new ArrayList<Phone>();
            phones2.addAll(Arrays.asList(new Phone[]{
                    new Phone("iPhone 8", "Apple", 52000),
                    new Phone("Nokia","9", 35000),
                    new Phone("Samsung", "Galaxy S9", 48000),
                    new Phone("HTC","U12", 36000)

            }));

            Phone min2 = phones2.stream().min(Phone::compare).get();
            Phone max2 = phones2.stream().max(Phone::compare).get();

            System.out.printf("MIN Name: %s Price: %d \n", min2.getName(), min2.getPrice());
            System.out.printf("MAX Name: %s Price: %d \n", max2.getName(), max2.getPrice());

        // ******* Метод reduce


        Stream<Integer> numbersStream = Stream.of(1,2,3,4,5,6);
        Optional<Integer> result = numbersStream.reduce((x,y)->x*y);
        System.out.println(result.get());


        Stream<String> wordStream = Stream.of("мама", "мыла", "раму");
//        Optional<String> sentence = wordStream.reduce((x,y)->x + " " + y);
//        System.out.println(sentence.get());

        String sentence1 = wordStream.reduce("Результат:", (x,y)->x + " " + y);
        System.out.println(sentence1);


        Stream<Phone> phoneStream4 = Stream.of(
                new Phone("iPhone 6S", "Apple", 54000),
                new Phone("Lumia","950", 45000),
                new Phone("Samsung", "Galaxy S6", 40000),
                new Phone("LG","G4", 32000));
        int sum = phoneStream4.reduce(0,
                (x,y)->{
            if(y.getPrice()<50000)
                return x+y.getPrice();
            else
                return x+0;
        },
                (x,y)->x+y);
        System.out.println(sum);

        // ******* Тип Optional

        ArrayList<Integer> numbers3 = new ArrayList<>();
//        numbers3.addAll(Arrays.asList(new Integer[]{1,2,3,4,5,6,7,8,9}));
        Optional<Integer> min3 = numbers3.stream().min(Integer::compare);
//        if(min3.isPresent()) {
//            System.out.println(min3.get());
//        }
        System.out.println(min3.orElse(-1));
        numbers3.addAll(Arrays.asList(new Integer[]{2,3,4,5,6,7,8,9}));
        min3 = numbers3.stream().min(Integer::compare);
        System.out.println(min3.orElse(-1));

        Random rnd = new Random();
        System.out.println(min3.orElseGet(()->rnd.nextInt(100)));

        System.out.println(min3.orElseThrow(IllegalStateException::new));

        min3.ifPresent(v-> System.out.println(v));

        min3.ifPresentOrElse(
                v-> System.out.println(v),
                ()-> System.out.println("Value not found")
        );


    }


}



